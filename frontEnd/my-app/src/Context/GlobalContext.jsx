import { createContext } from "react";
import { useAuth } from './../Hooks/useAuth';

const GlobalContext = createContext()


function GlobalProvider({children}){
    const {hadleLogin,user,token,isLogged} = useAuth()
    return(
        <GlobalContext.Provider value={{hadleLogin,user,token,isLogged}}>
            {children}
        </GlobalContext.Provider>
    );
}
export {GlobalContext, GlobalProvider}