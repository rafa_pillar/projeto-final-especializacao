import React  from "react";
import App from '../App';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Login from "../pages/page_login/index"
import { PageCurriculo } from "../Components/PageCurriculo/PageCurriculo";
import { PageTurmas } from './../Components/PageTurmas/PageTurmas';
import {PageHistoricos} from './../Components/PageHistoricos/PageHistoricos';
import { PageInscricao } from './../Components/PageInscricao/PageInscricao';
import { useContext } from 'react';
import { GlobalContext} from './../Context/GlobalContext';
import { Outlet  } from "react-router-dom";
import { PagePerfil } from './../Components/PagePerfil/PagePerfil';

export const Rotas = () => {
    const context = useContext(GlobalContext)

    function Logged() {
        const isLogged = context.isLogged
        console.log(isLogged)
        return isLogged ? <Outlet/> : <Navigate to="/login"/>
    }
    return(
        <BrowserRouter>
           <Routes>
            <Route path="/" element={<App />} >
                <Route path="/curriculo" element={<PageCurriculo />} />
                <Route path="/turmasAlunos" element={<PageTurmas />} />
                <Route path="/historicos" element={<PageHistoricos />} />
                <Route path="/inscricaoMaterias" element={<PageInscricao />} />
                <Route path="/curriculo" element={<PageCurriculo />} />
                <Route path="/perfil" element={<PagePerfil />} />
            </Route>
            <Route path="/login" element={<Login />} />
            <Route />
            </Routes>
        </BrowserRouter>
    );
}