import classes from './PageTurmas.module.css'
import { CardTurmas01 } from './../../Common/CardTurmas/CardTurmas01';

export function PageTurmas(){
    return(
        <>
            <div className={classes.containerBody}>
                   
                    <h1>Turmas</h1>
                    <p>Aluno: Fulano da Silva</p>
                <table>
                    
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Código</th>
                            <th>Calendário</th>
                            <th>Sala</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <CardTurmas01/>
                    </tbody>
                </table>
            </div>
        </>
    );
}