import classes from './PageHistoricos.module.css';
import { CardHistoricos01 } from './../../Common/CardHistoricos/CardHistoricos01';

export function PageHistoricos(){
    return(
        <>
            <div className={classes.containerBody}>
                    <h1>Histórico</h1>
                <table>
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Status</th>
                            <th>Nota</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <CardHistoricos01/>
                    </tbody>
                </table>
            </div>
        </>
    );
}