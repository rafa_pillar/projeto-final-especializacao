import classes from './PageTurmasProf.module.css'
import { CardNotasProf } from './../../Common/CardNotasProf/CardNotasProf';

export function PageTurmasProf(){
    return(
        <>
            <div className={classes.containerBodyProfNotas}>
                    <h1>Turmas</h1>
                    <h2>Calculo 1</h2>
                    <h2>Turma A1</h2>
                    <p>Professor:Fulano da Silva</p>
                    <p>Calendário:Seg e Qua, 14hrs as 16hrs</p>
                <table>
                    <thead>
                        <tr>
                            <th>Lançamento de notas</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <CardNotasProf/>
                    </tbody>
                </table>
            </div>
        </>
    );
}