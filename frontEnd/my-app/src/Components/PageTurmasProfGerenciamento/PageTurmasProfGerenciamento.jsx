import classes from './PageTurmasProfGerenciamento.module.css'
import { CardTurmas02 } from './../../Common/CardTurmas/CardTurmas02';

export function PageTurmasProfGerenciamento(){
    return(
        <>
            <div className={classes.containerBodyGerenciamentoProf}>
                   
                    <h1>Turmas</h1>
                    <p>Prof: Fulano da Silva</p>
                <table>
                    
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Código</th>
                            <th>Calendário</th>
                            <th>Sala</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <CardTurmas02/>
                    </tbody>
                </table>
            </div>
        </>
    );
}