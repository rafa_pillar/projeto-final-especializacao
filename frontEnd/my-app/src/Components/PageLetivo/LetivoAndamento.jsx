import classes from './LetivoAndamento.module.css';
import { CardLetivo02 } from '../../Common/CardLetivo/CardLetivo02';
export function LetivoAndamento(){
    return(
        <div className={classes.containerBodyLetivoPlan}>
            <h1>Periodo Letivo</h1>
            <div className={classes.descriptionPlan}>
                <p>Ano letivo:2020.2</p>
                <p>Status:Planejamento</p>
                <div className={classes.addMaterias}>
                    <div>
                        <p>Materias oferecidas pelo departamento</p>
                    </div>
                </div>
            </div>
            <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Area de conhecimento</th>
                            <th>Inscrições</th>
                            <th className={classes.end}>total de vagas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <CardLetivo02/>
                    </tbody>
                </table>
        </div>
    );
}