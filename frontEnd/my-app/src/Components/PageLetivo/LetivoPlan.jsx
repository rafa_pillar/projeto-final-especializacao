import { AdicionarMaterias } from '../../Common/Buttons/AdicionarMaterias';
import { CardLetivo01 } from '../../Common/CardLetivo/CardLetivo01';
import classes from './LetivoPlan.module.css';
export function LetivoPlan(){
    return(
        <div className={classes.containerBodyLetivoPlan}>
            <h1>Periodo Letivo</h1>
            <div className={classes.descriptionPlan}>
                <p>Ano letivo:2020.2</p>
                <p>Status:Planejamento</p>
                <div className={classes.addMaterias}>
                    <div>
                        <p>Materias oferecidas pelo departamento</p>
                    </div>
                    <div>
                        <AdicionarMaterias/>
                    </div>
                </div>
            </div>
            <table>
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Area de conhecimento</th>
                            <th>total de vagas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <CardLetivo01/>
                    </tbody>
                </table>
        </div>
    );
}