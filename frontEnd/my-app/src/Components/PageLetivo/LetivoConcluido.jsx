import { Planejar } from '../../Common/Buttons/Planejar';
import { CardLetivo03 } from '../../Common/CardLetivo/CardLetivo03';
import classes from './LetivoConcluido.module.css';

export function LetivoConcluido(){
    return(
        <div className={classes.containerBodyLetivoConcluido}>
            <h1>Periodo Letivo</h1>
            <div className={classes.descriptionConcluido}>
                <div className={classes.rowing}>
                    <div>
                        <p>Ano letivo:2020.2</p>
                        <p>Status:Planejamento</p>
                    </div>
                        <Planejar/>
                </div>
                
            </div>
                <div className={classes.adjustP}><p>Materias oferecidas pelo departamento</p></div>    
            <table>
                
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th></th>
                            <th></th>
                            <th>Area de conhecimento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <CardLetivo03/>
                    </tbody>
                </table>
        </div>
    );
}