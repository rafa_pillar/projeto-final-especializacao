import classes from './PageCurriculoCoordDep.module.css';
import { CurriculoSelectGlobal } from './../../Common/CurriculoSelectGlobal/CurriculoSelectGlobal';
import { AdicionarCurriculo } from '../../Common/Buttons/AdicionarCurriculo';
import { CardCurriculo02 } from './../../Common/CardCurriculo/CardCurriculo02';

export function PageCurriculoCoordDep() {
    return (
        <>
            <div className={classes.containerBodyCoordDep}>
                <h1>Visualizar curículos</h1>
                <p>Visualize aqui o currículo de qualquer um dos cursos da UFF.</p>
                <div className={classes.adicionarCurriculo}>                    
                        <div>
                            <CurriculoSelectGlobal />
                        </div>
                        <div>
                            <AdicionarCurriculo/>
                        </div>
                </div>
                {/* <table>
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Periodo</th>
                            <th>CHT</th>
                        </tr>
                    </thead>
                    <tbody>
                        {/* <CardCurriculo02 /> */}
                    {/* </tbody>
                </table> */} 
            </div>
        </>
    );
}