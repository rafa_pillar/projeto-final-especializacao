import React from 'react';
import classes from './Sidebar.module.css'
import EditarDadosButton from '../../Common/Buttons/EditarDados';

import historico from '../../imgs/historico.png';
import inscricao from '../../imgs/inscricao.png';
import turmas from '../../imgs/turmas.png';
import curriculos from '../../imgs/curriculos.png';
import logoUff from '../../imgs/logo da uff.png';
import { Link } from 'react-router-dom';

function Sidebar() {

  return (
    <container className={classes.sidebar}>
      <div>
        <div className={classes.blueBox}>
          <p className={classes.name}>Fulano da Silva</p>
          <p className={classes.course}>Sistemas de Informação</p>
          <p className={classes.codigo}>M. 163416515</p>
          <Link to="/perfil">
            <EditarDadosButton />
          </Link>
        </div>
        <div className={classes.whiteBox}>
          <ul>
            <li><img src={historico} alt="" /><p><Link to="/historicos" style={{textDecoration: "none", color: 'black'}}>Historico</Link></p></li>
            <li><img src={curriculos} alt="" /><p><Link to="/curriculo" style={{textDecoration: "none", color: 'black'}}>Curriculo</Link></p></li>
            <li><img src={turmas} alt="" /><p><Link to="/turmasAlunos" style={{textDecoration: "none", color: 'black'}}>Turmas</Link></p></li>
            <li><img src={inscricao} alt="" /><p><Link to="/inscricaoMaterias" style={{textDecoration: "none", color: 'black'}}>Inscrições Online</Link></p></li>
          </ul>
        </div>
      </div>
      <img src={logoUff} alt="" />
    </container>
  );

}

export default Sidebar;