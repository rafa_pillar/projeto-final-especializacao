import classes from './PagePerfil.module.css';

export function PagePerfil() {
    return (
        <>
            <form className={classes.containerBody}>
                <h1>Editar perfil</h1>
                <div className={classes.form}>
                    <div className={classes.dados}>
                        <div className={classes.inputName}>
                            <p>Edite os dados cadastrados para sua conta do IDUFF</p>
                            <p>Nome Completo</p>
                            <input type="text" />
                            <p>Nacionalidade: Brasil/BR</p>
                            <p>Estado: Rio de Janeiro</p>
                        </div>
                        <div>
                            <p>RG: 12.123.123-1</p>
                            <p>CPF: 123.123.123-11</p>
                            <p>Dados para contato</p>
                        </div>
                    </div>
                    <div className={classes.formContato}>
                        <div>
                            <div className={classes.inputRua}>
                                <p>Rua</p>
                                <input type="text" name="" id="" />
                            </div>
                            <div className={classes.inputNumero}>
                                <p>Nº</p>
                                <input type="text" name="" id="" />
                            </div>
                            <div className={classes.inputBairro}>
                                <p>Bairro</p>
                                <input type="text" name="" id="" />
                            </div>
                        </div>
                        <div>
                            <div className={classes.inputComplemento}>
                                <p>Complemento</p>
                                <input type="text" name="" id="" />
                            </div>
                            <div className={classes.inputEstado}>
                                <p>Estado</p>
                                <input type="text" name="" id="" />
                            </div>
                            <div className={classes.inputCep}>
                                <p>Cep</p>
                                <input type="text" name="" id="" />
                            </div>
                        </div>
                        <div>
                            <div className={classes.inputTelefone}>
                                <p>Telefone</p>
                                <input type="text" name="" id="" />
                            </div>
                            <div className={classes.inputCelular}>
                                <p>Celular</p>
                                <input type="text" name="" id="" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>
    )
}