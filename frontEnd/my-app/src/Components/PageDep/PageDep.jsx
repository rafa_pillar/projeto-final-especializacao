import { GerenciarMateriais } from '../../Common/Buttons/GerenciarMaterias';
import { Salvar } from '../../Common/Buttons/Salvar';
import { VisualizarAno } from '../../Common/Buttons/VisualizarAno';
import classes from './PageDep.module.css';
export  function PageDep(){
    return(
    <form className={classes.containerForm}>
            <h1>Departamento</h1>
        <div className={classes.alinhador}>
        <div className={classes.dados}>
                <p>Edite os informações do departamento</p>
            <div className={classes.rows}>
                <label for="depname">Nome do departamento:</label>
                <input type="text" id="depname" />
            </div>
            <div className={classes.rows}>
                <label for="conhecimento">Área do conhecimento:</label>
                <input type="text" id="conhecimento"/>
            </div>
            <div className={classes.rows}>
                <label for="campus">Campus sede:</label>
                <input type="text" id="campus"/>
            </div>
        </div>
        <div className={classes.contato}>
            <h2>Dados para contato:</h2>
            <div className={classes.inputcontatos}>
                <div className={classes.rows02}>
                    <p>Telefone:</p>
                    <input type="text" name="" id="" />
                </div>
                <div className={classes.rows02}>
                        <p>E-mail</p>
                        <input type="text" className={classes.email}name="" id="email" />
                </div>
            </div>   
            <div className={classes.allBtns}>  
                <div className={classes.buttons}>
                    <GerenciarMateriais/>   
                    <VisualizarAno/>
                </div>
                <div className={classes.buttonSave}>
                    <Salvar/>
                </div>
            </div>
        </div>
        </div>
        
    </form>
    );
}