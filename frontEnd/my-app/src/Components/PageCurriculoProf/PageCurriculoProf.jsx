import classes from './PageCurriculoProf.module.css';
import { CurriculoSelectGlobal } from './../../Common/CurriculoSelectGlobal/CurriculoSelectGlobal';
import { CardCurriculo01 } from './../../Common/CardCurriculo/CardCurriculo01';

export function PageCurriculoProf(){
    return(
        <>
            <div className={classes.containerBodyCurriculoProf}>
                    <h1>Visualizar curículos</h1>
                    <p>Visualize aqui o currículo de qualquer um dos cursos da UFF.</p>
                    <CurriculoSelectGlobal/>
                <table>
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Periodo</th>
                            <th>CHT</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <CardCurriculo01/>
                            <CardCurriculo01/>
                    </tbody>
                </table>
            </div>
        </>
    );
}