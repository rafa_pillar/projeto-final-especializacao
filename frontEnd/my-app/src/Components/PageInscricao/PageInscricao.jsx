import classes from './PageInscricao.module.css';
import { CardInscricao } from './../../Common/CardInscricao/CardInscricao';

export function PageInscricao(){
    return(
        <>
            <div className={classes.containerBody}>
                    <h1>Inscrição em matérias</h1>
                <table>
                    <thead>
                        <tr>
                            <th>NOME</th>
                            <th>Vagas</th>
                            <th>Período</th>
                            <th>CHT</th>
                        </tr>
                    </thead>
                    <tbody>                        
                            <CardInscricao/>
                    </tbody>
                </table>
            </div>
        </>
    );
}