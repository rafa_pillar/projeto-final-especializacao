import './App.css';
import React from 'react';
import { Outlet  } from "react-router-dom";
import Sidebar from './Components/Sidebar/Sidebar';
import {GlobalProvider} from './Context/GlobalContext';


function App() {
  return (
    <GlobalProvider>
      <div className='flex'>
        <Sidebar/>
        <Outlet/>
      </div>
    </GlobalProvider>
    );
}

export default App;
