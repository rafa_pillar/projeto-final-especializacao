import { useContext } from 'react';
import { GlobalContext } from '../../Context/GlobalContext';
import classes from './style.module.css';
import { useState } from 'react';



function Login(){
    const [cpf,setCpf] = useState('')
    const [password,setPassword] = useState('')

    const context = useContext(GlobalContext)

    let body ={
        "user":{
            "cpf": cpf,
            "password": password
        }
    }

    function handleForm(e) {
        e.preventDefault();
        context.handleLogin(body)
    }

    return (
        

        <div className={classes.container_login}>
            
            <div className={classes.container_login_description}>
                <h1 className={classes.container_title}>id<em className={classes.container_title_em} >UFF</em></h1>
                <h2 className={classes.container_subtitle}>Bem vindo ao Sistema <br/>Acadêmico da Graduação</h2>
                <p className={classes.container_description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis ex vel mauris porta pulvinar. Integer luctus bibendum ex et faucibus. Vivamus sagittis consectetur quam et rhoncus. Cras imperdiet nisi et elementum dictum. Duis nec turpis eleifend, viverra turpis et, egestas felis.</p>
            </div>

            <div className={classes.formulario_container__login}>
                <form className={classes.form_login}>
                    <h1 className={classes.formulario_title__login}>Acesse o seu id<em className={classes.container_title_em}>UFF</em></h1>
                    <div className={classes.form_div_input}>
                        <label htmlFor="cpf" className={classes.cpf}>CPF (Somente números)</label>
                        <input type="text" name="cpf" id="cpf" className={classes.cpf__input} onChange={(e) =>setCpf(e.target.value)} />
                    </div>
                    <div className={classes.form_div_input}>
                        <label htmlFor="password" className={classes.password}>Senha</label>
                            <input type="password" name="password" id="password" className={classes.password__input} onChange={(e) =>setPassword(e.target.value)}/>
                    </div>
                    
                        <button type="submit" className={classes.buttonLog}>Logar</button>
                        <span className={classes.forgotPassWord}>Esqueci minha senha</span>
                
                </form>
            </div>
        </div>
    
    );
}

  export default Login;