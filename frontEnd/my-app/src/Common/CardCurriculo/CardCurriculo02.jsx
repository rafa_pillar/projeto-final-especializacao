import classes from './CardCurriculo02.module.css';
export function CardCurriculo02(){
    <>
        <tr className={classes.trCard02Curriculo}>
            <td>calculo 1</td>
            <td>
                <select className={classes.periodos}>
                    <option value="0">1° Período</option>
                    <option value="1">2° Período</option>
                    <option value="2">3° Período</option>
                    <option value="3">4° Período</option>
                    <option value="4">5° Período</option>
                </select>
            </td>
            <td>60</td>
        </tr>
        
    </>
}