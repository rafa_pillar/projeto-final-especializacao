import classes from './CardNotasProf.module.css';
import { Enviar } from './../Buttons/Enviar';

export function CardNotasProf(){
    return(
        <tr className={classes.trCardProfNotas}>
                <td className={classes.nomeTd}>Jose dos santos</td>
                <td className={classes.notas}>
                    <input type="text" placeholder="Nota p1" className={classes.inputNota}/>
                    <input type="text" placeholder="Nota p2" className={classes.inputNota}/>
                </td>
                <td><Enviar/></td>
        </tr>
    );
}