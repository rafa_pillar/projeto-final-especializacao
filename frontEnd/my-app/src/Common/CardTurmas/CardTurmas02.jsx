import classes from './CardTurmas02.module.css'
import { Gerenciar } from './../Buttons/Gerenciar';

export function CardTurmas02(){
    return(
        <tr className={classes.trCardTurmasAluno02}>
                <td>Cálculo I</td>
                <td>A1</td>
                <td>Seg e Qua, 14h as 16h</td>
                <td>401</td>
                <td><Gerenciar/></td>
        </tr>
      
    );
}