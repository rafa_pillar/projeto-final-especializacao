import classes from './CardInscricao.module.css'
import { Inscrever } from '../Buttons/Inscrever';

export function CardInscricao(){
    return(
        <tr className={classes.trCardInscricao}>
                <td>Cálculo</td>
                <td>13/45</td>
                <td>1° periodo</td>
                <td>60</td>
                <td><Inscrever/></td>
        </tr>
      
    );
}