import classes from './Salvar.module.css'

export function Salvar(){
    return(
        <button className={classes.btnSalvar}>Salvar</button>
    );
}