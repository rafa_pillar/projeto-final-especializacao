import React from 'react';
import classes from './EditarDados.module.css'
import { Link } from 'react-router-dom';

function EditarDadosButton(props) {
    return (
        <button className={classes.btnEdit} ><Link to="/perfil" style={{textDecoration: "none", color: 'black'}}>editar dados</Link></button>
    )
}

export default EditarDadosButton;