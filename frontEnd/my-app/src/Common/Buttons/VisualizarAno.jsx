import classes from './VisualizarAno.module.css'

export function VisualizarAno(){
    return(
        <button className={classes.btnVisualizarAno}>Vizualizar Ano letivo</button>
    );
}