import classes from './CurriculoSelectGlobal.module.css'
export function CurriculoSelectGlobal(){
    return(
        <>
        <div className={classes.cursos_container}>
            <label for="global">Curso:</label>
            <select name="global" id="cursos">
                <option value="0"></option>
                <option value="1">Calculo</option>
                <option value="2">Geometria</option>
                <option value="3">MAT</option>
            </select>
        </div>
        </>
    );
}