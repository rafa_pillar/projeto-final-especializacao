import { useState } from 'react';
import { api } from './../Service/api';

export function useAuth(){
    const [user, setUser] = useState('')
    const [token,setToken] = useState('')
    const[isLogged,setIslogged] = useState(false)

    const hadleLogin = (body) =>{
        api.post('/login',body).then(res =>{
            setUser(res.data.user)
            setToken(res.data.token)
            localStorage.setItem('user',JSON.stringify(res.data.user))
            localStorage.setItem('token',JSON.stringify(res.data.token))
            console.log("teste")
            setIslogged(true)
        }).catch(err =>console.log(err))
    }
    return({hadleLogin, user,token, isLogged })
}