import axios from 'axios'

const api = axios.create({
    baseURL:'https://projeto-final-grupo-2.herokuapp.com'
})
export { api }